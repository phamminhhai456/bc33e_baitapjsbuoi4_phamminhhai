// Bài 1
document.getElementById("btnSapXep").onclick = function () {
  var soThuNhat = document.getElementById("soThu1").value * 1;
  var soThuHai = document.getElementById("soThu2").value * 1;
  var soThuBa = document.getElementById("soThu3").value * 1;

  var result1 = document.getElementById("ketQuaBai1");

  if (soThuNhat > soThuHai && soThuNhat > soThuBa) {
    if (soThuHai > soThuBa) {
      result1.innerHTML = `${soThuBa}, ${soThuHai}, ${soThuNhat} `;
    } else {
      result1.innerHTML = `${soThuHai}, ${soThuBa}, ${soThuNhat} `;
    }
  } else if (soThuHai > soThuNhat && soThuHai > soThuBa) {
    if (soThuNhat > soThuBa) {
      result1.innerHTML = `${soThuBa}, ${soThuNhat}, ${soThuHai} `;
    } else {
      result1.innerHTML = `${soThuNhat}, ${soThuBa}, ${soThuHai} `;
    }
  } else if (soThuBa > soThuNhat && soThuBa > soThuHai) {
    if (soThuNhat > soThuHai) {
      result1.innerHTML = `${soThuHai}, ${soThuNhat}, ${soThuBa} `;
    } else {
      result1.innerHTML = `${soThuNhat}, ${soThuHai}, ${soThuBa} `;
    }
  }
};

// Bài 2

document.getElementById("btnChao").onclick = function () {
  var chonThanhVien = document.getElementById("thanhVien").value;
  var thongBao = document.getElementById("ketQuaBai2");
  if (chonThanhVien == 1) {
    thongBao.innerHTML = `Xin chào Bố`;
  }
  if (chonThanhVien == 2) {
    thongBao.innerHTML = `Xin chào Mẹ`;
  }
  if (chonThanhVien == 3) {
    thongBao.innerHTML = `Xin chào Anh Trai`;
  }
  if (chonThanhVien == 4) {
    thongBao.innerHTML = `Xin chào Em Gái`;
  }
};

// Bài 3

document.getElementById("btnDem").onclick = function () {
  var soThuNhat = document.getElementById("so1").value * 1;
  var soThuHai = document.getElementById("so2").value * 1;
  var soThuBa = document.getElementById("so3").value * 1;

  var soChan = 0;

  if (soThuNhat % 2 == 0) {
    soChan++;
  }

  if (soThuHai % 2 == 0) {
    soChan++;
  }

  if (soThuBa % 2 == 0) {
    soChan++;
  }

  var soLe = 3 - soChan;

  document.getElementById(
    "ketQuaBai3"
  ).innerHTML = `Có ${soChan} số chẳn, ${soLe} số lẻ`;
};

// Bài 4

document.getElementById("btnDoan").onclick = function () {
  var canhThuNhat = document.getElementById("canh1").value * 1;
  var canhThuHai = document.getElementById("canh2").value * 1;
  var canhThuBa = document.getElementById("canh3").value * 1;

  var message = document.getElementById("ketQuaBai4");

  if (canhThuNhat == canhThuHai && canhThuHai == canhThuBa) {
    message.innerHTML = "Hình tam giác đều";
  } else if (
    canhThuNhat == canhThuHai ||
    canhThuHai == canhThuBa ||
    canhThuNhat == canhThuBa
  ) {
    message.innerHTML = "Hinh tam giác cân";
  } else if (
    canhThuNhat * canhThuNhat ==
      canhThuHai * canhThuHai + canhThuBa * canhThuBa ||
    canhThuHai * canhThuHai ==
      canhThuNhat * canhThuNhat + canhThuBa * canhThuBa ||
    canhThuBa * canhThuBa == canhThuNhat * canhThuNhat + canhThuHai * canhThuHai
  ) {
    message.innerHTML = "Hình tam giác vuông";
  } else {
    message.innerHTML = "Hình tam giác thường";
  }
};
